'use strict'

require('babel-polyfill')

let mainCanvas = document.createElement('canvas')
document.body.appendChild(mainCanvas)
Object.assign(document.body.style, {
  margin: '0', padding: '0',
  width: '100vw', height: '100vh',
  overflow: 'none'
})
Object.assign(mainCanvas.style, {
  position: 'absolute',
  left: '0', right: '0',
  top: '0', bottom: '0'
})

let ctx = mainCanvas.getContext('2d')
const valueUnitConvert = {
  pixelperlength: 2000,
  mspers: 1000
}
const constants = {
  mass: 0.005, // 5g
  G: -0.0001,
  coeFrictionToV: 1.2,
  forceThreshold: 50 * 1, // 2 m/s^2 * 1 kg
  gravityRPower: 2,
  ugCoe: 1,
  ugThreshold: 20,
  ugUserSpeedPower: 0.9,
  ugRAdd: 0.1
}
const cache = {
  pixelperlength2: valueUnitConvert.pixelperlength * valueUnitConvert.pixelperlength
}
let gState = {
  state: null,
  points: [],
  viewport: {
    translate: [0, 0]
  },
  paused: false,
  dt: 0.02 * valueUnitConvert.mspers,
  lastTime: null,
  touches: []
}
function client2vp (x, y) {
  let vp = gState.viewport
  return [x - vp.translate[0], y - vp.translate[1]]
}
function vp2client (x, y) {
  let vp = gState.viewport
  return [x + vp.translate[0], y + vp.translate[1]]
}
function client2vpScale (x, y) {
  return [x, y]
}
function mainGameLoop () {
  requestAnimationFrame(mainGameLoop)
  let {innerWidth: width, innerHeight: height} = window
  if (!gState.viewSize || gState.viewSize[0] !== width || gState.viewSize[1] !== height) {
    Object.assign(mainCanvas, {width, height})
    gState.viewSize = [width, height]
  }
  if (!gState.state) {
    gState.viewport.translate = [width / 2, height / 2]
    return void Object.assign(gState, {
      state: 'generating',
      gen: {
        centerX: 0,
        centerY: 0,
        r: 50,
        n: 50,
        nextGenTime: Date.now() + 1000 / 60
      },
      lastTime: Date.now()
    })
  }
  if (gState.state === 'generating') {
    let { gen } = gState
    if (gState.points.length >= gen.n) {
      return void Object.assign(gState, {
        state: 'normal',
        gen: null,
        genDone: Date.now(),
        paused: false
      })
    }
    if (Date.now() >= gen.nextGenTime) {
      gen.nextGenTime = Date.now() + 1000 / 60
      let { centerX, centerY, r: rMax } = gen
      let r = Math.random() * rMax
      let deg = 2*Math.PI*Math.random()
      let nPoint = {
        x: Math.cos(deg)*r + centerX,
        y: Math.sin(deg)*r + centerY,
        birth: Date.now(),
        vX: 0,
        vY: 0
      }
      gState.points.push(nPoint)
    }
  }

  if (!gState.paused) {
    let tRem = (Date.now() - gState.lastTime) / valueUnitConvert.mspers
    let dt = gState.dt / valueUnitConvert.mspers
    while (tRem > dt) {
      tRem -= dt
      gState.lastTime += gState.dt
      let points = gState.points
      for (let p of points) {
        p.forceX = p.aX = 0
        p.forceY = p.aY = 0
      }

      // Apply (anti-)gravity
      for (let pointI = 0; pointI < points.length; pointI ++) {
        for (let pointJ = pointI + 1; pointJ < points.length; pointJ ++) {
          let pA = points[pointI]
          let pB = points[pointJ]
          let r2inM = Math.max(0.000001 /* (1 mm)^2 */, (Math.pow(pA.x - pB.x, 2) + Math.pow(pA.y - pB.y, 2)) / cache.pixelperlength2)
          let gravForce = (Math.pow(constants.mass, 2) * constants.G) / Math.pow(r2inM, constants.gravityRPower / 2)
          let angleAB = Math.atan2(pB.y - pA.y, pB.x - pA.x)
          pA.forceX += Math.cos(angleAB) * gravForce
          pA.forceY += Math.sin(angleAB) * gravForce
          pB.forceX -= Math.cos(angleAB) * gravForce
          pB.forceY -= Math.sin(angleAB) * gravForce
        }
      }

      // Apply friction

      for (let point of points) {
        let vMag2InM = Math.pow(point.vX, 2) + Math.pow(point.vY, 2)
        let vMagInM = Math.sqrt(vMag2InM)
        if (vMagInM < 0.001 /*1 mm / s*/) continue
        let fMag = constants.coeFrictionToV * vMag2InM
        point.forceX -= (point.vX / vMagInM) * fMag
        point.forceY -= (point.vY / vMagInM) * fMag
      }

      // Apply user drag
      for (let ug of gState.touches) {
        if (!ug) continue
        let [centerX, centerY] = client2vp(ug.lastX, ug.lastY)
        let [dx, dy] = client2vpScale(ug.dx, ug.dy)
        let fScale2 = dx * dx + dy * dy + 1
        fScale2 /= cache.pixelperlength2
        fScale2 *= 100
        let fScale = Math.pow(fScale2, constants.ugUserSpeedPower / 2)
        for (let point of gState.points) {
          let r2InM = Math.max(0.000001, (Math.pow(point.x - centerX, 2) + Math.pow(point.y - centerY, 2)) / cache.pixelperlength2)
          if (r2InM > 0.003) continue
          let fMag = constants.mass * constants.ugCoe * fScale / (constants.ugRAdd + r2InM)
          fMag = Math.min(constants.ugThreshold, fMag)
          let angle = Math.atan2(centerY - point.y, centerX - point.x)
          point.forceX += Math.cos(angle) * fMag
          point.forceY += Math.sin(angle) * fMag
        }
        ug.dx = ug.dy = 0
      }

      // Apply forces, acceleration and velocity
      for (let point of points) {
        if (Math.abs(point.forceX) > constants.forceThreshold) {
          point.forceX = Math.sign(point.forceX) * constants.forceThreshold
        }
        if (Math.abs(point.forceY) > constants.forceThreshold) {
          point.forceY = Math.sign(point.forceY) * constants.forceThreshold
        }
        point.aX += point.forceX / constants.mass
        point.aY += point.forceY / constants.mass
        point.vX += point.aX * dt
        point.vY += point.aY * dt
        point.x += point.vX * dt * valueUnitConvert.pixelperlength
        point.y += point.vY * dt * valueUnitConvert.pixelperlength
        let sum = point.x + point.y
        if (!Number.isFinite(sum) || Number.isNaN(sum)) {
          gState.paused = true
          console.error('Invalid point.')
        }
      }
      constants.coeFrictionToV *= Math.pow(0.95, dt)
      constants.ugCoe *= Math.pow(0.99, dt)
    }
  } else {
    gState.lastTime = Date.now()
  }

  ctx.save()
    ctx.fillStyle = 'white'
    ctx.fillRect(0, 0, width, height)
  ctx.restore()

  let numInScene = 0
  ctx.save()
    ctx.translate(...gState.viewport.translate)
    ctx.globalAlpha = 1
    for (let point of gState.points) {
      let prog = Math.min(1, Math.max(0, (Date.now() - point.birth) / 500))
      prog = Math.sqrt(prog)
      ctx.save()
        ctx.fillStyle = '#416bf2'
        ctx.beginPath()
        ctx.arc(point.x, point.y, prog * 5, 0, 2*Math.PI)
        let [vpX, vpY] = vp2client(point.x, point.y)
        if (vpX > 0 && vpY > 0 && vpX < width && vpY < height) numInScene ++
        ctx.fill()
      ctx.restore()
    }
  ctx.restore()

  let genDoneProg = 1
  if (gState.state === 'generating') {
    genDoneProg = 0
  } else {
    genDoneProg = (Date.now() - gState.genDone) / 300
  }
  genDoneProg = Math.max(0, genDoneProg)
  if (genDoneProg < 0.999) {
    ctx.save()
      ctx.fillStyle = '#fff'
      ctx.globalAlpha = 0.5 * (1 - genDoneProg)
      ctx.fillRect(0, 0, width, height)
      if (genDoneProg === 0) {
        ctx.fillStyle = '#000'
        ctx.globalAlpha = 1
        ctx.textAlign = 'center'
        ctx.font = '18px serif'
        ctx.fillText('Generating points, please wait\u2026', width / 2, height / 2 - 18)
      }
    ctx.restore()
  }

  if (genDoneProg > 0) {
    ctx.save()
      ctx.textAlign = 'left'
      ctx.fillStyle = '#000'
      ctx.font = '20px serif'
      ctx.globalAlpha = genDoneProg
      if (numInScene > 0) {
        ctx.fillText(`Friction: ${Math.round(constants.coeFrictionToV * 100) / 100}. Playing time: ${Math.floor((Date.now() - gState.genDone) / 1000)}s. ${numInScene} dots still under control.`, 10, height - 22, width - 20)
      } else {
        if (!gState.emptyTime) gState.emptyTime = Date.now()
        ctx.fillText(`Played for ${Math.floor((gState.emptyTime - gState.genDone) / 1000)}s. All dots escaped.`, 10, height - 22, width - 20)
        gState.paused = true
      }
    ctx.restore()
  }
}
mainGameLoop()

window.gState = gState

document.addEventListener('touchstart', evt => {
  for (let t of evt.touches) {
    gState.touches[t.identifier] = {
      lastX: t.clientX,
      lastY: t.clientY,
      dx: 0,
      dy: 0,
      end: false
    }
  }
}, {passive: false})

document.addEventListener('touchmove', evt => {
  evt.preventDefault()
  for (let t of evt.changedTouches) {
    let to = gState.touches[t.identifier]
    to.dx += t.clientX - to.lastX
    to.lastX = t.clientX
    to.dy += t.clientY - to.lastY
    to.lastY = t.clientY
    to.end = false
  }
}, {passive: false})

document.addEventListener('touchend', evt => {
  evt.preventDefault()
  for (let t of evt.changedTouches) {
    gState.touches[t.identifier] = null
    delete gState.touches[t.identifier]
  }
}, {passive: false})

document.addEventListener('touchcancel', evt => {
  evt.preventDefault()
  for (let t of evt.changedTouches) {
    gState.touches[t.identifier] = null
    delete gState.touches[t.identifier]
  }
}, {passive: false})
